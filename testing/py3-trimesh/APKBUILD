# Contributor: Aiden Grossman <agrossman154@yahoo.com>
# Maintainer: Aiden Grossman <agrossman154@yahoo.com>
pkgname=py3-trimesh
pkgver=3.16.0
pkgrel=0
pkgdesc="Python library for working with triangular meshes"
url="https://github.com/mikedh/trimesh"
# x86, armhf, armv7 Tests fail on int64 to int32 casts on these arches
# s390x, no py3-rtree
# riscv64, no py3-shapely
arch="noarch !x86 !armhf !armv7 !s390x !riscv64"
license="MIT"
depends="
	py3-colorlog
	py3-jsonschema
	py3-lxml
	py3-msgpack
	py3-networkx
	py3-pillow
	py3-requests
	py3-rtree
	py3-scipy
	py3-shapely
	py3-svgpath
	python3
	"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-pytest-xdist"
source="$pkgname-$pkgver.tar.gz::https://github.com/mikedh/trimesh/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/trimesh-$pkgver"

build() {
	python3 setup.py build
}

check() {
	pytest -n $JOBS \
		--deselect tests/test_dae.py::DAETest::test_material_round \
		--deselect tests/test_dae.py::DAETest::test_obj_roundtrip \
		--deselect tests/test_light.py::LightTests::test_scene
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
7b56a5007614eae8a7cef1a69334ef3b21499572b5bfaa414bd29097a431266e1ec4d7dda540dbf502a57f5b0ce9d3fb3763675821714ac25d924e0d15e22c71  py3-trimesh-3.16.0.tar.gz
"
